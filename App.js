import { StatusBar } from 'expo-status-bar';
import React, {useState, useRef } from 'react';

import { StyleSheet, Text, View, Image, ImageBackground, Button, TextInput, SafeAreaView,TouchableOpacity } from 'react-native';

// const image = {uri: 'https://reactjs.org/logo-og.png'};

export default function App() {

  const sampleGoals = [
    "Faire les courses",
    "Aller à la salle de sport 3 ,fois par semaine",
    "Monter à plus de 5000m d'altitude",
    "Acheter mon premier appartement",
    "Perdre 5 kgs",
    "Gagner en productivité",
    "Apprendre mon premier appartement",
    "Faire une mission en freelance",
    "Organiser un meetup autour de la tech",
    "Faire un triathlon",
    ];

  // const image = {uri: 'C:\Users\Administrateur\Desktop\Application1\image_background.png'};

    const [text, setText] = useState(null);
    const [list, setList] = useState(sampleGoals);
    
  //   const appui = () => {
  //     <Text style={styles.text1}>C'est ma premiere application! </Text>
  //   }
    
  //   const reinitAdd = useRef(null);

    const onPressAdd = (text) => {
      setList([...list,text]);
      console.log("liste ", list);
    }
    const deleteElement = () => {
      console.log("delete");
    }

  return (
      <SafeAreaView style={styles.container}>
      <Text style={styles.text1}>C'est ma premiere application! </Text>
      <Text style={styles.text2}>Apps</Text>
      <Text style={styles.text3}>C'est ma premiere application! </Text>

        {
          list.map((element, index) => {
            return(
              <View>
                <Text>{element}</Text>
                {/* setData(data => data.filter((item, i) => item !== element)); */}
                <Button
                style={styles.delete}
                title="x"
                color="#841584"
                accessibilityLabel="Del">
                </Button>
              </View>
            )
            
          })
        }
      {/* <ImageBackground source={image} resizeMode="cover" style={styles.imageBack}></ImageBackground> */}
      {/* <Image source={image}></Image> */}
     
      <TextInput
      style={styles.input}
      onChange={(e) => setText(e.nativeEvent.text)}
      ></TextInput>

      <Button
      onPress={
        () => onPressAdd(text)
      }
      style={styles.delete}
      title="Add"
      color="#841584"
      accessibilityLabel="Add">
      </Button>
      
      <StatusBar style="auto" />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text1: {
    color: '#ff0',
  },
  
  text2: {
    color: '#f0f',
    // fontWeight: "Bold",
  },

  text3: {
    color: '#ff0',
  },
  imageBack: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  delete: {
    alignSelf: 'flex-end',
    height: 40,
    width: 40,
  },
})